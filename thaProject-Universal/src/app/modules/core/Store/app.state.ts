import { Ingredient } from '../../../models/ingredient';
import { shoppingListReducer } from '../../shopping-list/store/shopping-list.reducers';
import { authReducer } from '../../auth/store/auth.reducers';
import { ActionReducerMap } from '@ngrx/store';

export const reducerMappings: ActionReducerMap<AppState> = {
    shoppingList: shoppingListReducer,
    auth: authReducer
};

export class AppState {
    shoppingList: {
        ingredients: Ingredient[],
        editedIngredientId: number
    };
    auth: {
        currentUserEmail: string,
        userToken: string,
        isAuthenticated: boolean
    };
}
