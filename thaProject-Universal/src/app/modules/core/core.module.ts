import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';
import { RecipeResolverService } from './guards/recipe-resolver.service';
import { AuthenticationGuard } from './guards/authentication.guard';
import { AuthService } from './services/auth.service';
import { RecipeService } from './services/recipe.service';
import { AuthInterceptor } from './Interceptors/auth.interceptor';
import { reducerMappings } from './Store/app.state';
import { AuthEffects } from '../auth/store/auth.effects';
import { environment } from '../../../environments/environment';
import { RecipeListResolverService } from './guards/recipe-list-resolver.service';

let dev = [
  StoreDevtoolsModule.instrument()
];

if (environment.production) {
  dev = [];
}

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    StoreModule.forRoot(reducerMappings),
    EffectsModule.forRoot([AuthEffects]),
    StoreRouterConnectingModule,
    ...dev
  ],
  declarations: [
    HeaderComponent,
    HomeComponent
  ],
  exports: [
    AppRoutingModule,
    HeaderComponent,
    StoreModule
  ],
  providers: [
    RecipeResolverService,
    RecipeListResolverService,
    AuthenticationGuard,
    AuthService,
    RecipeService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}
  ]
})
export class CoreModule { }
