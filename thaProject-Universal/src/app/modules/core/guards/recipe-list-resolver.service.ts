import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { RecipeService } from '../services/recipe.service';

@Injectable()
export class RecipeListResolverService implements Resolve<any> {

  constructor(private recipeServie: RecipeService) { }

  resolve(): Observable<any> | Promise<any> | any {

    return this.recipeServie.initRecipes();
  }

}
