import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { AppState } from '../Store/app.state';


@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(private store: Store<AppState>,
              private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.store.select('auth')
        .pipe(
          take(1),
          map(
          (authState) => {
            if (authState.isAuthenticated) {
              return true;
            } else {
              this.router.navigate(['/signin']);
            }
          }
        ));
  }
}
