import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, EMPTY } from 'rxjs';

import { take, mergeMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../Store/app.state';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private store: Store<AppState>) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes('https://thaproject-angular-course.firebaseio.com')) {
            return this.store.select('auth')
                .pipe(take(1),
                    mergeMap(
                        (authState) => {
                            if (authState.isAuthenticated) {
                                req = req.clone({ params: req.params.set('auth', authState.userToken)});
                                return next.handle(req);
                            } else {
                                return EMPTY;
                            }
                        }
                ));
        } else {
            return next.handle(req);
        }
    }

}
