import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';

import { RecipeListComponent } from './components/recipe-book/recipe-list/recipe-list.component';
import { RecipeItemComponent } from './components/recipe-book/recipe-list/recipe-item/recipe-item.component';
import { RecipeDetailsComponent } from './components/recipe-book/recipe-details/recipe-details.component';
import { NewRecipeComponent } from './components/recipe-book/new-recipe/new-recipe.component';
import { EditRecipeComponent } from './components/recipe-book/edit-recipe/edit-recipe.component';
import { RecipesRoutingModule } from './recipes-routing.module';
import { RecipeBookComponent } from './components/recipe-book/recipe-book.component';
import { SharedModule } from '../shared/shared.module';
import { recipeReducer } from './store/recipe.reducers';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RecipesRoutingModule,
    SharedModule,
    StoreModule.forFeature('recipes', recipeReducer)
  ],
  declarations: [
    RecipeBookComponent,
    RecipeListComponent,
    RecipeItemComponent,
    RecipeDetailsComponent,
    NewRecipeComponent,
    EditRecipeComponent,
  ]
})
export class RecipesModule { }
