import { Recipe } from '../../../models/recipe';
import * as RecipeActions from './recipe.actions';

export interface FeatureState {
    recipes: State;
}

export interface State {
    recipes: Recipe[];
}

const initialState: State = {
    recipes: []
};

export function recipeReducer(state = initialState, action: RecipeActions.actions): State {
    switch (action.type) {
        case RecipeActions.SET_RECIPES:
            return getRecipesSetState(state, action.payload);
        case RecipeActions.ADD_RECIPE:
            return getRecipeAddedState(state, action.payload);
        case RecipeActions.UPDATE_RECIPE:
            return getRecipeUpdatedState(state, action.payload.index, action.payload.recipe);
        case RecipeActions.DELETE_RECIPE:
            return getRecipeDeletedState(state, action.payload);
        default:
            return state;
    }
}

function getRecipesSetState(initState: State, recipes: Recipe[]) {
    const state = {...initState, recipes: [...initState.recipes]};
    state.recipes = [...recipes];
    return state;
}

function getRecipeAddedState(initState: State, recipe: Recipe) {
    const state = {...initState, recipes: [...initState.recipes]};
    state.recipes = [...state.recipes, recipe];
    return state;
}

function getRecipeUpdatedState(initState: State, index: number, recipe: Recipe) {
    const state = {...initState, recipes: [...initState.recipes]};
    state.recipes[index] = {...recipe};
    return state;
}

function getRecipeDeletedState(initState: State, index: number) {
    const state = {...initState, recipes: [...initState.recipes]};
    state.recipes.splice(index, 1);
    return state;
}
