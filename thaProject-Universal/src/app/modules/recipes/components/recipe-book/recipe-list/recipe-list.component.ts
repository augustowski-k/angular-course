import {
  Component,
  OnInit} from '@angular/core';

import { Recipe } from '../../../../../models/recipe';
import { Store } from '@ngrx/store';
import * as fromRecipe from '../../../store/recipe.reducers';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[];

  constructor(private store: Store<fromRecipe.FeatureState>) { }

  ngOnInit() {
    this.store.select('recipes')
      .subscribe(
        recipeState => {
          this.recipes = recipeState.recipes;
        }
      );
  }
}
