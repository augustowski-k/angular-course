import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { Store } from '@ngrx/store';

import { Recipe } from '../../../../../models/recipe';
import * as ShoppingListActions from '../../../../shopping-list/store/shopping-list.actions';
import { AppState } from '../../../../core/Store/app.state';
import * as fromRecipe from '../../../store/recipe.reducers';
import * as RecipeActions from '../../../store/recipe.actions';



@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe: Recipe;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<AppState>,
              private recipeStore: Store<fromRecipe.FeatureState>) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.recipe = data['recipe'];
    });
  }

  addIngredientsToList() {
    this.store.dispatch(new ShoppingListActions.AddIngredients(this.recipe.ingredients));
  }

  onRemoveRecipe() {
    this.recipeStore.dispatch(
      new RecipeActions.DeleteRecipe(Number(this.route.snapshot.params['id']))
    );
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
