import { Action } from '@ngrx/store';

export const TRY_SIGNUP = 'TRY_SIGNUP';
export const TRY_SIGNIN = 'TRY_SIGNIN';
export const INIT_WITH_USER = 'INIT_WITH_USER';
export const SIGNIN = 'SIGNIN';
export const TRY_SIGNOUT = 'TRY_SIGNOUT';
export const SIGNOUT = 'SIGNOUT';
export const SET_TOKEN = 'SET_TOKEN';

export class TrySignup implements Action {
    readonly type = TRY_SIGNUP;

    constructor(public payload: {email: string, password: string}) {}
}

export class TrySignin implements Action {
    readonly type = TRY_SIGNIN;

    constructor(public payload: {email: string, password: string}) {}
}

export class InitWithUser implements Action {
    readonly type = INIT_WITH_USER;

    constructor(public payload: {userEmail: string}) {}
}

export class Signin implements Action {
    readonly type = SIGNIN;

    constructor(public payload: {userEmail: string}) {}
}

export class TrySignout implements Action {
    readonly type = TRY_SIGNOUT;
}

export class Signout implements Action {
    readonly type = SIGNOUT;
}

export class SetToken implements Action {
    readonly type = SET_TOKEN;

    constructor(public payload: string) {}
}

export type actions = TrySignup | TrySignin | InitWithUser | Signin | Signout | SetToken;
