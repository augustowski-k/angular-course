import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { from } from 'rxjs';
import { map, switchMap, mergeMap, tap } from 'rxjs/operators';
import * as firebase from 'firebase';

import * as AuthActions from './auth.actions';
import { Router } from '@angular/router';
import { RecipeService } from '../../core/services/recipe.service';

@Injectable()
export class AuthEffects {
    @Effect()
    authSignup = this.actions$
        .ofType(AuthActions.TRY_SIGNUP)
        .pipe(
            map(
                (action: AuthActions.TrySignup) => {
                    return action.payload;
            }),
            switchMap(
                (authData: {email: string, password: string}) => {
                    return from(firebase.auth().createUserWithEmailAndPassword(authData.email, authData.password));
            }),
            switchMap(
                () => {
                    return from(firebase.auth().currentUser.getIdToken());
            }),
            mergeMap(
                (token) => {
                    return [
                        {
                            type: AuthActions.SET_TOKEN,
                            payload: token
                        },
                        {
                            type: AuthActions.SIGNIN,
                            payload: {userEmail: firebase.auth().currentUser.email}
                        }
                    ];
            })
        );

    @Effect()
    authSignin = this.actions$
        .ofType(AuthActions.TRY_SIGNIN)
        .pipe(
            map(
                (action: AuthActions.TrySignin) => {
                    return action.payload;
            }),
            switchMap(
                (authData: {email: string, password: string}) => {
                    return from(firebase.auth().signInWithEmailAndPassword(authData.email, authData.password));
            }),
            switchMap(
                () => {
                    return from(firebase.auth().currentUser.getIdToken());
            }),
            mergeMap(
                (token) => {
                    return [
                        {
                            type: AuthActions.SET_TOKEN,
                            payload: token
                        },
                        {
                            type: AuthActions.SIGNIN,
                            payload: {userEmail: firebase.auth().currentUser.email}
                        }
                    ];
            })
        );

    @Effect(
        {dispatch: false}
    )
    authSignedin = this.actions$
        .ofType(AuthActions.SIGNIN)
        .pipe(
            tap(
                () => {
                    this.recipeService.fetchRecipes();
                    this.router.navigate(['/recipes']);
            })
        );

    @Effect()
    authSignOut = this.actions$
        .ofType(AuthActions.TRY_SIGNOUT)
        .pipe(
            switchMap(
                () => {
                    return from(firebase.auth().signOut());
                }
            ),
            map(
                () => {
                    return {type: AuthActions.SIGNOUT};
                }
            )
        );

    @Effect(
        {dispatch: false}
    )
    authSignedout = this.actions$
        .ofType(AuthActions.SIGNOUT)
        .pipe(
            tap(
                () => {
                    this.router.navigate(['/']);
            })
        );

    constructor(private actions$: Actions,
                private router: Router,
                private recipeService: RecipeService) {}
}
