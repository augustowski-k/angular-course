import * as AuthActions from './auth.actions';
import { AppState } from '../../core/Store/app.state';

const initialState: AppState['auth'] = {
    currentUserEmail: null,
    userToken: null,
    isAuthenticated: false
};

export function authReducer(state: AppState['auth'] = initialState, action: AuthActions.actions): AppState['auth'] {
    switch (action.type) {
        case AuthActions.SIGNIN:
        case AuthActions.INIT_WITH_USER:
            return getSignedInState(state, action.payload.userEmail);
        case AuthActions.SIGNOUT:
            return getSignedOutState(state);
        case AuthActions.SET_TOKEN:
            return getTokenSetState(state, action.payload);
        default:
            return {...state};
    }
}

function getSignedInState(initState: AppState['auth'], email: string): AppState['auth'] {
    const state = {...initState};
    state.isAuthenticated = true;
    state.currentUserEmail = email;
    return state;
}

function getSignedOutState(initState: AppState['auth']): AppState['auth'] {
    const state = {...initState, ...initialState};
    return state;
}

function getTokenSetState(initState: AppState['auth'], token: string): AppState['auth'] {
    const state = {...initState};
    state.userToken = token;
    return state;
}
