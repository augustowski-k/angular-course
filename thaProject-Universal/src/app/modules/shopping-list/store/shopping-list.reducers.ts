import { Ingredient } from '../../../models/ingredient';
import * as ShoppingListActions from './shopping-list.actions';
import { AppState } from '../../core/Store/app.state';



const initialState: AppState['shoppingList'] = {
    ingredients: [
        new Ingredient('flour', 2, 'kg'),
        new Ingredient('tomato', 4, 'pc')
    ],
    editedIngredientId: null
};

export function shoppingListReducer(state: AppState['shoppingList'] = initialState, action: ShoppingListActions.Actions) {
    switch (action.type) {
        case ShoppingListActions.ADD_INGREDIENT:
            return getIngredientAddedState(state, action.payload);
        case ShoppingListActions.ADD_INGREDIENTS:
            return getIngredientsAddedState(state, action.payload);
        case ShoppingListActions.UPDATE_INGREDIENT:
            return getIngredientUpdatedState(state, action.payload);
        case ShoppingListActions.REMOVE_INGREDIENT:
            return getIngredientRemovedState(state, action.payload);
        case ShoppingListActions.TOGGLE_EDIT:
            return getEditToggledState(state, action.payload);
        default:
            return state;
    }
}

function getIngredientAddedState(initState: AppState['shoppingList'], ingredient: Ingredient) {
    const state = {...initState, ingredients: [...initState.ingredients]};
    addIngredient(state.ingredients, ingredient);
    return state;
}

function getIngredientsAddedState(initState: AppState['shoppingList'], ingredients: Ingredient[]) {
    const state = {...initState, ingredients: [...initState.ingredients]};
    ingredients.forEach(
        (ingr) => addIngredient(state.ingredients, ingr));
    return state;
}

function getIngredientUpdatedState(initState: AppState['shoppingList'], payload: {ingredient: Ingredient, index: number}) {
    const state = {...initState, ingredients: [...initState.ingredients]};
    state.ingredients[payload.index] = payload.ingredient;
    if (state.editedIngredientId === payload.index) {
        state.editedIngredientId = null;
    }
    return state;
}

function getIngredientRemovedState(initState: AppState['shoppingList'], index: number) {
    const state = {...initState, ingredients: [...initState.ingredients]};
    if (state.editedIngredientId === index) {
        state.editedIngredientId = null;
    }
    state.ingredients.splice(index, 1);
    return state;
}

function getEditToggledState(initState: AppState['shoppingList'], index: number) {
    const state = {...initState};
    state.editedIngredientId = state.editedIngredientId !== index ? index : null;
    return state;
}

function addIngredient(ingrArray: Ingredient[], ingredient: Ingredient) {
    const foundIngredient = ingrArray.find(ingr => ingr.name === ingredient.name);
    if ( foundIngredient ) {
        foundIngredient.amount += ingredient.amount;
    } else {
        ingrArray.push(ingredient);
    }
}
