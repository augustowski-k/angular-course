import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from './modules/core/services/auth.service';
import { RecipeService } from './modules/core/services/recipe.service';

export function init_auth(authService: AuthService) {
  return () => authService.initialize();
}

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    {
      provide: APP_INITIALIZER, useFactory: init_auth, deps: [AuthService], multi: true
    },
  ]
})
export class AppInitModule { }
