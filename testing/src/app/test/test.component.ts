import { Component, OnInit } from '@angular/core';
import { TestService } from '../services/test.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css'],
  providers: [
    TestService,
    DataService
  ]
})
export class TestComponent implements OnInit {
  user: {name: string};
  isLoggedIn: boolean;
  data: string;
  
  constructor(private testService: TestService,
              private dataService: DataService) { }

  ngOnInit() {
    this.user = this.testService.user;
    this.dataService.getDetails().then((data: string) => {
      this.data = data;
    })
  }

}
