import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestComponent } from './test.component';
import { TestService } from '../services/test.service';
import { DataService } from '../services/data.service';

describe('TestComponent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent
      ]
    })
  });

  it('should create the component', () => {
    let fixture = TestBed.createComponent(TestComponent);
    let app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should init with user named Max', () => {
    let fixture = TestBed.createComponent(TestComponent);
    let app = fixture.debugElement.componentInstance;
    fixture.detectChanges();
    expect(app.user.name).toEqual('Max');
  });

  it(`should display name 'Max' when user is logged in`, () => {
    let fixture = TestBed.createComponent(TestComponent);
    let app = fixture.debugElement.componentInstance;
    app.isLoggedIn = true;
    fixture.detectChanges();
    let compiled = fixture.debugElement.nativeElement;

    expect(compiled.querySelector('p').textContent).toContain('Max');
  });

  it(`should fetch data successfully`, async(() => {
    let fixture = TestBed.createComponent(TestComponent);
    let app = fixture.debugElement.componentInstance;
    let dataService = fixture.debugElement.injector.get(DataService);
    let spy = spyOn(dataService, 'getDetails')
      .and.returnValue(Promise.resolve('test string here'));
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(app.data).toEqual('test string here');   
    }); 
  }));
});
