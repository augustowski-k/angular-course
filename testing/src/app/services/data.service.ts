import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  constructor() { }

  getDetails() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('test data string');
      }, 1000);
    });
  }
}
