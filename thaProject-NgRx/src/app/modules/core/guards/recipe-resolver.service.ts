import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { Recipe } from '../../../models/recipe';
import * as fromRecipe from '../../recipes/store/recipe.reducers';
import * as RecipeActions from '../../recipes/store/recipe.actions';

@Injectable()
export class RecipeResolverService implements Resolve<Recipe> {

  constructor(private store: Store<fromRecipe.FeatureState>,
              private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Promise<Recipe> | Recipe {
    return this.store.select('recipes')
      .pipe(
        take(1),
        map(
          recipeState => {
            const recipe = recipeState.recipes[Number(route.params['id'])];
            if (recipe) {
              return recipe;
            }
            this.router.navigate(['/recipes', 'not-found']);
          }
        )
      );
  }

}
