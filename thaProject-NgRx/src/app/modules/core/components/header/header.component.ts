import {
  Component,
  OnInit
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { RecipeService } from '../../services/recipe.service';
import { AppState } from '../../Store/app.state';
import * as AuthActions from '../../../auth/store/auth.actions';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  authState: Observable<AppState['auth']>;

  constructor(private recipeService: RecipeService,
              private store: Store<AppState>) { }

  ngOnInit() {
    this.authState = this.store.select('auth');
  }

  onUploadData() {
    this.recipeService.uploadRecipes();
  }

  onFetchData() {
    this.recipeService.fetchRecipes();
  }

  onSignOut() {
    this.store.dispatch(
      new AuthActions.TrySignout()
    );
  }
}
