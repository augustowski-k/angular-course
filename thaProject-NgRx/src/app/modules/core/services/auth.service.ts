import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject, from } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from '../Store/app.state';
import * as AuthActions from '../../auth/store/auth.actions';

@Injectable()
export class AuthService {
  private _isInitialized = false;
  private _initObserver;

  get isInitialized() {
    return this._isInitialized;
  }

  private readonly CONFIG = {
    apiKey: 'AIzaSyAhvFTnwCrpFKfywAQ7YeANOCi78mhElIo',
    authDomain: 'thaproject-angular-course.firebaseapp.com',
  };

  private authInitialized = new Subject<any>();

  constructor(private store: Store<AppState>) { }

  private setInitialized() {
    this._isInitialized = true;
    this._initObserver();
    this.authInitialized.next();
  }

  initialize() {
    const that = this;
    return new Promise(
      (resolve) => {
        firebase.initializeApp(that.CONFIG);
        that._initObserver = firebase.auth().onAuthStateChanged(function(user) {
          if (user) {
            that.store.dispatch(
              new AuthActions.InitWithUser({userEmail: user.email})
            );
            that.refreshToken()
              .then(that.setInitialized.bind(that));
          } else {
            that.setInitialized();
          }
        });
        resolve();
      }
    );
  }

  callWhenInitialized(func, args?) {
    return new Promise(
      (resolve) => {
        if (this._isInitialized) {
          resolve(func(...args));
        } else {
          this.authInitialized.subscribe(
            () => {
              resolve(func(...args));
            }
          );
        }
      }
    );
  }

  refreshToken() {
    return firebase.auth().currentUser.getIdToken()
      .then(
        (token) => {
          this.store.dispatch(
            new AuthActions.SetToken(token)
          );
        }
      );
  }

}
