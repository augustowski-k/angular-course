import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, defaultIfEmpty, take, switchMap, catchError } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Recipe } from '../../../models/recipe';
import { Store } from '@ngrx/store';
import * as fromRecipe from '../../recipes/store/recipe.reducers';
import * as RecipeActions from '../../recipes/store/recipe.actions';

@Injectable()
export class RecipeService {

  constructor(private httpClient: HttpClient,
              private authService: AuthService,
              private store: Store<fromRecipe.FeatureState>) {}

  uploadRecipes() {
    this.store.select('recipes')
      .pipe(
        take(1),
        switchMap(
          (recipeState) => {
            return this.httpClient
              .put(
                'https://thaproject-angular-course.firebaseio.com/recipes.json',
                recipeState.recipes);
          }
      ))
      .subscribe();
  }

  initRecipes() {
    return this.authService.callWhenInitialized(this.fetchRecipes.bind(this));
  }

  fetchRecipes() {
    return this.httpClient
      .get<Recipe[]>(
        'https://thaproject-angular-course.firebaseio.com/recipes.json')
      .pipe(
        defaultIfEmpty([]),
        map(
          (recipes) => this.ensureRecipesIngredients(recipes) ))
      .toPromise()
      .then(
        (recipes: Recipe[]) => {
          this.store.dispatch(
            new RecipeActions.SetRecipes(recipes)
          );
      })
      .catch(
        (err) => console.log(err) );
  }

  ensureRecipesIngredients(recipes) {
    for (const recipe of recipes) {
      if (!recipe.ingredients) {
        recipe.ingredients = [];
      }
    }
    return recipes;
  }

}
