import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DropdownDirective } from './directives/dropdown.directive';
import { InfoDisplayComponent } from './components/info-display/info-display.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    DropdownDirective,
    InfoDisplayComponent
  ],
  exports: [
    DropdownDirective
  ]
})
export class SharedModule { }
