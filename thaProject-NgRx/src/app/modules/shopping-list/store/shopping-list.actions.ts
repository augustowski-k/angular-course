import { Action } from '@ngrx/store';
import { Ingredient } from '../../../models/ingredient';

export const ADD_INGREDIENT = 'ADD_INGREDIENT';
export const ADD_INGREDIENTS = 'ADD_INGREDIENTS';
export const UPDATE_INGREDIENT = 'UPDATE_INGREDIENT';
export const REMOVE_INGREDIENT = 'REMOVE_INGREDIENT';
export const TOGGLE_EDIT = 'TOGGLE_EDIT';

export class AddIngredient implements Action {
    readonly type = ADD_INGREDIENT;

    constructor(public payload: Ingredient) {}
}

export class AddIngredients implements Action {
    readonly type = ADD_INGREDIENTS;

    constructor(public payload: Ingredient[]) {}
}

export class UpdateIngredient implements Action {
    readonly type = UPDATE_INGREDIENT;

    constructor(public payload: {ingredient: Ingredient, index: number}) {}
}

export class RemoveIngredient implements Action {
    readonly type = REMOVE_INGREDIENT;

    constructor(public payload: number) {}
}

export class ToggleEdit implements Action {
    readonly type = TOGGLE_EDIT;

    constructor(public payload: number) {}
}

export type Actions = AddIngredient | AddIngredients | UpdateIngredient | RemoveIngredient | ToggleEdit;
