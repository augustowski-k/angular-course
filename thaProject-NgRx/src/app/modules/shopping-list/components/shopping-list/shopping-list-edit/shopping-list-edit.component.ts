import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { Ingredient } from '../../../../../models/ingredient';
import * as ShoppingListActions from '../../../store/shopping-list.actions';
import { AppState } from '../../../../core/Store/app.state';


@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit, OnDestroy {
  private storeSubscription: Subscription;

  @ViewChild('f') ingredientForm: NgForm;
  shoppingListState: AppState['shoppingList'];

  get editEnabled() {
    return this.shoppingListState.editedIngredientId != null;
  }

  get currentEdited() {
    return this.editEnabled ? this.shoppingListState.ingredients[this.shoppingListState.editedIngredientId] : null;
  }

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.storeSubscription = this.store.select('shoppingList')
      .subscribe(
        (shoppingListState) => {
          if ( !this.shoppingListState || shoppingListState.editedIngredientId !== this.shoppingListState.editedIngredientId) {
            this.shoppingListState = shoppingListState;
            this.updateView();
          }
        }
    );
  }

  updateView() {
    if ( this.editEnabled ) {
      this.ingredientForm.form.patchValue({
        name: this.currentEdited.name,
        amount: this.currentEdited.amount,
        unit: this.currentEdited.unit
      });
    } else {
      this.ingredientForm.reset();
    }
  }

  onSubmit() {
    const formValues = this.ingredientForm.value;
    const ingredient = new Ingredient(formValues.name, formValues.amount, formValues.unit);

    if (this.editEnabled) {
      this.store.dispatch(
        new ShoppingListActions.UpdateIngredient({
          ingredient: ingredient,
          index: this.shoppingListState.editedIngredientId
        }));
    } else {
      this.store.dispatch(new ShoppingListActions.AddIngredient(ingredient));
      this.ingredientForm.reset();
    }
  }

  onDelete() {
    if (this.editEnabled) {
      this.store.dispatch(
        new ShoppingListActions.RemoveIngredient(this.shoppingListState.editedIngredientId)
      );
    }
  }

  onClear() {
    this.ingredientForm.reset();
  }

  ngOnDestroy() {
    if (this.storeSubscription) {
      this.storeSubscription.unsubscribe();
    }
  }

}
