import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import { AppState } from '../../../core/Store/app.state';
import * as ShoppingListActions from '../../store/shopping-list.actions';



@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  shoppingListState: Observable<AppState['shoppingList']>;


  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.shoppingListState = this.store.select('shoppingList');
  }

  onItemClicked(index: number) {
    this.store.dispatch(
      new ShoppingListActions.ToggleEdit(index)
    );
  }

}
