import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { RecipeDetailsComponent } from './components/recipe-book/recipe-details/recipe-details.component';
import { EditRecipeComponent } from './components/recipe-book/edit-recipe/edit-recipe.component';
import { NewRecipeComponent } from './components/recipe-book/new-recipe/new-recipe.component';
import { RecipeBookComponent } from './components/recipe-book/recipe-book.component';
import { InfoDisplayComponent } from '../shared/components/info-display/info-display.component';
import { AuthenticationGuard } from '../core/guards/authentication.guard';
import { RecipeResolverService } from '../core/guards/recipe-resolver.service';

const recipesRoutes: Routes = [
  { path: '', component: RecipeBookComponent, children: [
    { path: '', component: InfoDisplayComponent, pathMatch: 'full', data: { message: 'Please select a recipe' }},
    {
        path: 'new',
        component: NewRecipeComponent,
        canActivate: [AuthenticationGuard]
    },
    { path: 'not-found', component: InfoDisplayComponent, data: { message: 'The recipe you were trying to reach does not exist'}},
    {
        path: ':id/edit',
        component: EditRecipeComponent,
        resolve: {recipe: RecipeResolverService},
        canActivate: [AuthenticationGuard]
    },
    { path: ':id', component: RecipeDetailsComponent, resolve: {recipe: RecipeResolverService} }
  ]}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(recipesRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class RecipesRoutingModule { }
