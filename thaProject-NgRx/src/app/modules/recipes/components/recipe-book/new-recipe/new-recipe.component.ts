import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { Recipe } from '../../../../../models/recipe';
import * as fromRecipe from '../../../store/recipe.reducers';
import * as RecipeActions from '../../../store/recipe.actions';


@Component({
  selector: 'app-new-recipe',
  templateUrl: './new-recipe.component.html',
  styleUrls: ['./new-recipe.component.css']
})
export class NewRecipeComponent implements OnInit {
  newRecipeForm: FormGroup;

  get ingredients(): FormArray {
    return this.newRecipeForm.get('ingredients') as FormArray;
  }

  constructor(private store: Store<fromRecipe.FeatureState>,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.newRecipeForm = new FormGroup({
      'name': new FormControl(null,
        Validators.required),
      'description': new FormControl(null,
        Validators.required),
      'imagePath': new FormControl(null,
        Validators.required),
      'ingredients': new FormArray([])
    });
  }

  onSubmit() {
    const newRecipe = new Recipe(
      this.newRecipeForm.value.name,
      this.newRecipeForm.value.description,
      this.newRecipeForm.value.imagePath,
      this.newRecipeForm.value.ingredients);

    this.store.dispatch(
      new RecipeActions.AddRecipe(newRecipe)
    );
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  onRemoveIngredient(index: number) {
    this.ingredients.removeAt(index);
  }

  onAddIngredient() {
    this.ingredients.push(new FormGroup({
      'name': new FormControl(null, Validators.required),
      'amount': new FormControl(null, Validators.required),
      'unit': new FormControl('pc')
    }));
  }

  onClear() {
    this.ingredients.controls = [];
    this.newRecipeForm.reset();
  }
}
