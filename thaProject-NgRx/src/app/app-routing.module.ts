import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { HomeComponent } from './modules/core/components/home/home.component';
import { RecipeListResolverService } from './modules/core/guards/recipe-list-resolver.service';


const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'recipes', resolve: {recipes: RecipeListResolverService}, loadChildren: './modules/recipes/recipes.module#RecipesModule'}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule {

}
