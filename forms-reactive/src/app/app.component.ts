import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  genders = ['male', 'female'];
  forbiddenUsernames = ['overlord', 'beggar'];

  signupForm: FormGroup;

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.signupForm = new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null, [
            Validators.required,
            this.forbiddenNames.bind(this)
        ], [
          this.nameTaken
        ]),
        'email': new FormControl(null, [
            Validators.required,
            Validators.email
        ])
      }),
      'gender': new FormControl('male'),
      'hobbies': new FormArray([])
    });
  }

  onSubmit() {
    console.log(this.signupForm);
  }

  onAddHobby() {
    const hobbyControl = new FormControl(null, Validators.required);
    (<FormArray>this.signupForm.get('hobbies'))
      .push(hobbyControl);
  }

  forbiddenNames(control: FormControl): {[s: string]: boolean} {
    if (this.forbiddenUsernames.includes(control.value)) {
      return {'nameIsForbidden': true};
    }
    return null;
  }

  nameTaken(control: FormControl): Promise<any> | Observable<any> {
    return new Promise<any>(
      (resolve, reject) => {
        setTimeout(
          () => {
            if (control.value === 'taken') {
              resolve({'nameIsTaken': true});
            } else {
              resolve(null);
            }
          }, 1500);
      }
    );
  }

}
