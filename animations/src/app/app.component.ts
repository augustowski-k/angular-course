import { Component } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [
    trigger('divState', [
      state('normal', style({
        backgroundColor: 'red',
        transform: 'translateX(0)'
      })),
      state('highlighted', style({
        backgroundColor: 'blue',
        transform: 'translateX(100px)'
      })),
      transition(
        'normal <=> highlighted', animate(200))
    ]),
    trigger('list', [
      transition('void => *', [
        style({
          opacity: 0,
          transform: "translateX(-100px)"
        }),
        animate(400)
      ]),
      transition('* => void', [
        animate(400, style({
          opacity: 0,
          transform: "translateX(100px)"
        }))
      ])
    ])
  ]
})
export class AppComponent {
  state = 'normal';
  list = ['Milk', 'Sugar', 'Bread'];

    onAdd(item) {
      this.list.push(item);
    }

    onDelete(item){
      const index = this.list.indexOf(item);
      this.list.splice(index, 1);
    }

    onAnimate(){
      this.state = this.state === 'normal' ? 'highlighted' : 'normal';
    }
}
