import { Component, OnInit, OnDestroy } from '@angular/core';

import 'rxjs-compat/Rx';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  numberObsSub: Subscription;
  myObsSub: Subscription;
  constructor() { }

  ngOnInit() {
    const numbers = Observable.interval(1000)
      .map(
        (data: number) => {
          return data * 2;
        }
      );
    this.numberObsSub = numbers.subscribe(
      (number: number) => {
        console.log(number);
      }
    );

    const myObs = Observable.create(
      (observer: Observer<any>) => {
        setTimeout(() => {
          observer.next('first package');
        }, 2000);
        setTimeout(() => {
          observer.next('second package');
        }, 4000);
        setTimeout(() => {
          observer.error('does not work');
        }, 5000);
        setTimeout(() => {
          observer.next('third pckg');
        }, 6000);
      }
    );
    this.myObsSub = myObs.subscribe(
      (data: string) => {
        console.log(data);
      },
      (err: string) => {
        console.error(err);
      },
      () => {
        console.log('completed');
      }
    );
  }

  ngOnDestroy(): void {
    if (this.myObsSub) {
      this.myObsSub.unsubscribe();
    }
    if (this.numberObsSub) {
      this.numberObsSub.unsubscribe();
    }
  }

}
