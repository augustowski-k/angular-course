import { Injectable } from '@angular/core';
import 'rxjs-compat/Rx';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  userActivated = new Subject();

  constructor() { }
}
