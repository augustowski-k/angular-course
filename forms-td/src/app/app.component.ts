import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('f') signupForm: NgForm;

  secretAnswer: string;
  genders = [
    'male',
    'female'
  ];

  user = {
    name: '',
    email: '',
    secretQuestion: '',
    secretAnswer: '',
    gender: ''
  };
  submitted: boolean;

  suggestUserName() {
    const suggestedName = 'Superuser';

    this.signupForm.form.patchValue({
      userData: {
        username: suggestedName
      },
      gender: 'female'
    });
  }

  onSubmit(form: NgForm) {
    console.log(form);
    this.user.name = form.value.userData.username;
    this.user.email = form.value.userData.email;
    this.user.gender = form.value.gender;
    this.user.secretAnswer = form.value.secretAnswer;
    this.user.secretQuestion = form.value.secret;
    this.submitted = true;

    this.signupForm.reset();
  }

}
