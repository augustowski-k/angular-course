import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { CounterService } from '../services/counter.service';
import { User } from '../models/user';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent implements OnInit {
  users: User[];

  constructor(private userService: UsersService) {}


  ngOnInit() {
    this.users = this.userService.getActiveUsers();
    this.userService.usersStateChanged.subscribe(
      () => {
        this.users = this.userService.getActiveUsers();
      }
    );
  }

  onSetToInactive(user: User) {
    this.userService.toggleActive(user);
  }
}
