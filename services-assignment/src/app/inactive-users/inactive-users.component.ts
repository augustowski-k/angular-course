import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { CounterService } from '../services/counter.service';
import { User } from '../models/user';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent implements OnInit {
  users: User[];

  constructor(private userService: UsersService) {}


  ngOnInit() {
    this.users = this.userService.getInactiveUsers();
    this.userService.usersStateChanged.subscribe(
      () => {
        this.users = this.userService.getInactiveUsers();
      }
    );
  }

  onSetToActive(user: User) {
    this.userService.toggleActive(user);
  }
}
