import { Component, OnInit } from '@angular/core';
import { CounterService } from './services/counter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  activationsCount: number;
  deactivationsCount: number;
  constructor(private counter: CounterService) {}

  ngOnInit() {
    this.activationsCount = this.counter.getActivationsCount();
    this.deactivationsCount = this.counter.getDeactivationsCount();

    this.counter.counterChanged.subscribe(
      (activation) => {
        if (activation) {
          this.activationsCount = this.counter.getActivationsCount();
        } else {
          this.deactivationsCount = this.counter.getDeactivationsCount();
        }
      }
    );
  }
}
