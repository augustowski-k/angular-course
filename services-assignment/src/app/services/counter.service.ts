import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  public counterChanged = new EventEmitter<boolean>();
  private activationCounter = 0;
  private deactivationCounter = 0;

  public getActivationsCount() {
    return this.activationCounter;
  }

  public getDeactivationsCount() {
    return this.deactivationCounter;
  }

  public countActivation() {
    this.activationCounter++;
    this.counterChanged.emit(true);
  }

  public countDeactivation() {
    this.deactivationCounter++;
    this.counterChanged.emit(false);
  }

  constructor() { }
}
