import { Injectable, EventEmitter } from '@angular/core';
import { CounterService } from './counter.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public usersStateChanged = new EventEmitter<any>();
  private users: User[] = [
    new User('Max', true),
    new User('Anna', true),
    new User('Chris', false),
    new User('Manu', false),
    new User('Tristan', true)
  ];

  constructor(private counter: CounterService) { }

  public getActiveUsers() {
    return this.users.filter(u => u.active);
  }

  public getInactiveUsers() {
    return this.users.filter(u => !u.active);
  }

  public toggleActive(inputUser: User) {
    const id = this.users.findIndex(u => u.name === inputUser.name);
    const user = this.users[id];
    user.active = !user.active;
    if (user.active) {
      this.counter.countActivation();
    } else {
      this.counter.countDeactivation();
    }
    this.usersStateChanged.emit();
  }
}
