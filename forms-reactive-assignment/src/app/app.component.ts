import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  statusOptions = [
    'Stable',
    'Critical',
    'Finished'
  ];

  projectForm: FormGroup;

  ngOnInit(): void {
    this.initializeForm();
  }

  initializeForm() {
    this.projectForm = new FormGroup({
      'projectName': new FormControl(null, [
        Validators.required,
        this.validName
      ], [
        this.validNameAsync
      ]),
      'contactEmail': new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      'status': new FormControl(null)
    });
  }

  onSubmit() {
    console.log(this.projectForm.valid);
    console.log(this.projectForm.value);
  }

  validName(control: FormControl): {[s: string]: boolean} {
    return control.value === 'test'
      ? {'nameIsInvalid': true}
      : null;
  }

  validNameAsync(control: FormControl): Promise<any> | Observable<any> {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            if (control.value === 'async') {
              resolve({'nameIsInvalid': true});
            } else {
              resolve(null);
            }
        }, 2000);
      }
    );
  }

}
