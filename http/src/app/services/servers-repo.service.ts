import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs-compat/Rx';
import { Observable } from 'rxjs-compat/Rx';

@Injectable({
  providedIn: 'root'
})
export class ServersRepoService {

  constructor(private http: Http) { }

  storeServers(servers: any[]) {
    return this.http.put('https://angular-http-course-module.firebaseio.com/serv.json', servers);
  }

  getServers() {
    return this.http.get('https://angular-http-course-module.firebaseio.com/serv.json')
      .map(
        (response: Response) => {
          const data = response.json();
          for (const server of data) {
            server.name = 'Fetched_' + server.name;
          }
          return data;
        })
      .catch(
        (error: Response) => {
          return Observable.throw('something went wrong');
        }
      );
  }

  getAppName() {
    return this.http.get('https://angular-http-course-module.firebaseio.com/appName.json')
      .map(
        (res: Response) => {
          return res.json();
        }
      );
  }
}
