import { Component, OnInit } from '@angular/core';
import { ServersRepoService } from './services/servers-repo.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  appName = this.serverRepo.getAppName();
  servers = [];

  onAddServer(name: string) {
    this.servers.push({
      name: name,
      capacity: 50,
      id: this.generateId()
    });
  }

  constructor(private serverRepo: ServersRepoService) {}

  private generateId() {
    return Math.round(Math.random() * 10000);
  }

  ngOnInit() {
    this.serverRepo.getServers().subscribe(
      (servers: any[]) => {
        this.servers = servers;
      },
      (error) => {
        console.warn(error);
      }
    );
  }

  onUploadServers() {
    this.serverRepo.storeServers(this.servers)
      .subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.warn(error);
        }
      );
  }
}
