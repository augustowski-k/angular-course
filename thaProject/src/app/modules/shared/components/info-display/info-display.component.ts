import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { componentFactoryName } from '@angular/compiler';

@Component({
  selector: 'app-info-display',
  templateUrl: './info-display.component.html',
  styleUrls: ['./info-display.component.css']
})
export class InfoDisplayComponent implements OnInit {
  text: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.text = this.route.snapshot.data['message'];
  }

}
