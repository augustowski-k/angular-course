import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

import { Recipe } from '../../../../../models/recipe';
import { Ingredient } from '../../../../../models/ingredient';
import { RecipeService } from '../../../../core/services/recipe.service';


@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.css']
})
export class EditRecipeComponent implements OnInit {
  editRecipeForm: FormGroup;
  get ingredients() {
    return this.editRecipeForm.get('ingredients') as FormArray;
  }

  recipe: Recipe;

  constructor(private recipeService: RecipeService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.recipe = data['recipe'];
        this.initForm();
    });
  }

  initForm() {
    this.editRecipeForm = new FormGroup({
      'name': new FormControl(this.recipe.name,
        Validators.required),
      'description': new FormControl(this.recipe.description,
        Validators.required),
      'imagePath': new FormControl(this.recipe.imagePath,
        Validators.required),
      'ingredients': new FormArray(this.recipe.ingredients.map(ingr => this.createIngredientControls(ingr)))
    });
  }

  createIngredientControls(ingredient?: Ingredient) {
    return new FormGroup({
      'name': new FormControl((ingredient && ingredient.name) || null,
        Validators.required),
      'amount': new FormControl((ingredient && ingredient.amount) || null,
        Validators.required),
      'unit': new FormControl((ingredient && ingredient.unit) || 'pc')
    });
  }

  onAddIngredient() {
    this.ingredients.push(this.createIngredientControls());
  }

  onRemoveIngredient(index: number) {
    this.ingredients.removeAt(index);
  }

  onSubmit() {
    const newRecipe = new Recipe(
      this.editRecipeForm.value.name,
      this.editRecipeForm.value.description,
      this.editRecipeForm.value.imagePath,
      this.editRecipeForm.value.ingredients);
    const recipeId = Number(this.route.snapshot.params['id']);
    this.recipeService.updateRecipe(recipeId, newRecipe);
    this.router.navigate(['..'], {relativeTo: this.route});
  }

  onCancel() {
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
