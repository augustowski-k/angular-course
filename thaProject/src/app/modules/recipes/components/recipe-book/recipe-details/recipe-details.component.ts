import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';

import { Recipe } from '../../../../../models/recipe';
import { ShoppingListService } from '../../../../core/services/shopping-list.service';
import { RecipeService } from '../../../../core/services/recipe.service';



@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe: Recipe;

  constructor(private shoppingListService: ShoppingListService,
              private route: ActivatedRoute,
              private recipeService: RecipeService,
              private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(
      (data: Data) => {
        this.recipe = data['recipe'];
    });
  }

  addIngredientsToList() {
    this.recipe.ingredients.forEach(
      (ingredient) => {
        this.shoppingListService.addIngredient(ingredient);
      }
    );
  }

  onRemoveRecipe() {
    this.recipeService.removeRecipe(Number(this.route.snapshot.params['id']));
    this.router.navigate(['..'], {relativeTo: this.route});
  }
}
