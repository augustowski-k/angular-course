import {
  Component,
  OnInit} from '@angular/core';

import { Recipe } from '../../../../../models/recipe';
import { RecipeService } from '../../../../core/services/recipe.service';
import { AuthService } from '../../../../core/services/auth.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  get isAuthenticated() {
    return this.authService.isAuthenticated;
  }
  recipes: Recipe[];

  constructor(private recipeService: RecipeService,
              private authService: AuthService) { }

  ngOnInit() {
    this.recipes = this.recipeService.getRecipes();
    this.recipeService.recipesChanged
    .subscribe(
      () => {
        this.recipes = this.recipeService.getRecipes();
      }
    );
  }
}
