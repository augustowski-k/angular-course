import {
  Component,
  OnInit
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { RecipeService } from '../../services/recipe.service';
import { AuthService } from '../../services/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  get isAuthenticated() {
    return this.authService.isAuthenticated;
  }

  constructor(private recipeService: RecipeService,
              private authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
  }

  onUploadData() {
    this.recipeService.uploadRecipes();
  }

  onFetchData() {
    this.recipeService.fetchRecipes();
  }

  onSignOut() {
    this.authService.singoutUser();
    this.router.navigate(['/'], {relativeTo: this.route});
  }
}
