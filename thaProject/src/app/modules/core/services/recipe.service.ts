import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthService } from './auth.service';
import { Recipe } from '../../../models/recipe';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<any>();

  private recipes: Recipe[];

  constructor(private httpClient: HttpClient,
              private authService: AuthService) {}

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next();
  }

  getRecipes() {
    if (this.recipes) {
      return this.recipes.slice();
    } else {
      return [];
    }
  }

  getRecipe(id: number) {
    if (this.recipes) {
      return JSON.parse(JSON.stringify(this.recipes[id]));
    } else {
      return null;
    }
  }

  updateRecipe(id: number, recipe: Recipe) {
    for (const property of Object.keys(recipe)) {
      if (recipe[property]) {
        this.recipes[id][property] = recipe[property];
      }
    }
    this.recipesChanged.next();
  }

  removeRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next();
  }

  uploadRecipes() {
    this.httpClient
      .put(
        'https://thaproject-angular-course.firebaseio.com/recipes.json',
        this.recipes)
      .subscribe(
        () => { },
        (err) => { console.warn(err); }
      );
  }

  initRecipes() {
    return this.authService.callWhenInitialized(this.fetchRecipes.bind(this));
  }

  fetchRecipes() {
    return this.httpClient
      .get<Recipe[]>(
        'https://thaproject-angular-course.firebaseio.com/recipes.json')
      .pipe(map(
        recipes => this.ensureRecipesIngredients(recipes) ))
      .toPromise()
      .then(
        (recipes: Recipe[]) => {
          this.recipes = recipes;
          this.recipesChanged.next(); })
      .catch(
        (err) => console.log('err: ' + err) );
  }

  ensureRecipesIngredients(recipes) {
    for (const recipe of recipes) {
      if (!recipe.ingredients) {
        recipe.ingredients = [];
      }
    }
    return recipes;
  }

}
