import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { Subject, from } from 'rxjs';

@Injectable()
export class AuthService {
  get isAuthenticated() {
    return firebase.auth().currentUser !== null
        && firebase.auth().currentUser !== undefined;
  }

  get isInitialized() {
    return this._isInitialized;
  }

  private readonly CONFIG = {
    apiKey: 'AIzaSyAhvFTnwCrpFKfywAQ7YeANOCi78mhElIo',
    authDomain: 'thaproject-angular-course.firebaseapp.com',
  };

  private _isInitialized = false;
  private _initObserver;

  private authInitialized = new Subject<any>();

  constructor() { }

  private setInitialized() {
    this._isInitialized = true;
    this._initObserver();
    this.authInitialized.next();
  }

  initialize() {
    const that = this;
    return new Promise(
      (resolve) => {
        firebase.initializeApp(that.CONFIG);
        that._initObserver = firebase.auth().onAuthStateChanged(function(user) {
          that.setInitialized();
        });
        resolve();
      }
    );
  }

  callWhenInitialized(func, args?) {
    return new Promise(
      (resolve) => {
        if (this._isInitialized) {
          resolve(func(...args));
        } else {
          this.authInitialized.subscribe(
            () => {
              resolve(func(...args));
            }
          );
        }
      }
    );
  }

  signupUser(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
      .catch(
        err => console.log(err)
      );
  }

  signinUser(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        res => console.log(res)
      )
      .catch(
        err => {
          console.log(err);
          throw err;
        }
      );
  }

  singoutUser() {
    firebase.auth().signOut();
  }

  getToken() {
    const currentUser = firebase.auth().currentUser;
    if (currentUser) {
      return from(currentUser.getIdToken());
    } else {
      return from(new Promise((resolve) => resolve(null)));
    }
  }

}
