import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Ingredient } from '../../../models/ingredient';

@Injectable()
export class ShoppingListService {
  public ingredientListChanged = new Subject<any>();
  public editIngredientToggled = new Subject<number>();

  private ingredients: Ingredient[] = [
    new Ingredient('flour', 2, 'kg'),
    new Ingredient('tomato', 4, 'pc')
  ];

  getIngredientList() {
    return this.ingredients.slice();
  }

  getIngredient(index: number) {
    return JSON.parse(JSON.stringify(
      this.ingredients[index]));
  }

  addIngredient(ingredient: Ingredient) {
    const foundIngredient = this.ingredients.find((i) => i.name.toLowerCase() === ingredient.name.toLowerCase());
    if (foundIngredient) {
      foundIngredient.amount += ingredient.amount;
    } else {
      this.ingredients.push(ingredient);
    }
    this.ingredientListChanged.next();
  }

  updateIngredient(index: number, ingredient: Ingredient) {
    this.ingredients[index] = ingredient;
    this.ingredientListChanged.next();
  }

  removeIngredient(index: number) {
    this.editIngredientToggled.next(index);
    this.ingredients.splice(index, 1);
    this.ingredientListChanged.next();
  }

  onEditIngredient(index: number) {
    this.editIngredientToggled.next(index);
  }
}
