import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Recipe } from '../../../models/recipe';
import { RecipeService } from '../services/recipe.service';

@Injectable()
export class RecipeResolverService implements Resolve<Recipe> {

  constructor(private recipeService: RecipeService,
              private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Recipe> | Promise<Recipe> | Recipe {
    const recipe = this.recipeService.getRecipe(route.params['id']);
    if (recipe) {
      return recipe;
    }
    this.router.navigate(['/recipes', 'not-found']);
  }

}
