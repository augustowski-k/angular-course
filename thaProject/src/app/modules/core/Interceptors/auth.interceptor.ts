import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { from } from 'rxjs';

import { AuthService } from '../services/auth.service';
import { mergeMap } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (req.url.includes('https://thaproject-angular-course.firebaseio.com')) {
            return this.authService.getToken()
                .pipe(mergeMap(
                    (token: string) => {
                        req = req.clone({ params: req.params.set('auth', token)});
                        return next.handle(req);
                    }
                ));
        }
    }

}
