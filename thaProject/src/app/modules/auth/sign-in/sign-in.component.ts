import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RecipeService } from '../../core/services/recipe.service';
import { AuthService } from '../../core/services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  signInForm: FormGroup;

  constructor(private authService: AuthService,
              private router: Router,
              private recipesService: RecipeService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.signInForm = new FormGroup({
      'email': new FormControl(null, [
        Validators.required,
        Validators.email
      ]),
      'password': new FormControl(null,
        Validators.required)
    });
  }

  onSubmit() {
    this.authService.signinUser(this.signInForm.value.email, this.signInForm.value.password)
      .then(
        () => {
          this.recipesService.fetchRecipes();
          this.router.navigate(['/recipes']);
        }
      ).catch(
        () => this.signInForm.reset()
      );
  }

}
