import {
  Component,
  OnInit,
  ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms';

import { Ingredient } from '../../../../models/ingredient';
import { ShoppingListService } from '../../../core/services/shopping-list.service';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.css']
})
export class ShoppingListEditComponent implements OnInit {
  @ViewChild('f') ingredientForm: NgForm;
  editState = {
    active: false,
    index: null
  };

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.shoppingListService.editIngredientToggled
      .subscribe(
        (index) => {
          if (this.editState.active && this.editState.index === index) {
            this.disableEdit();
          } else {
            this.enableEdit(index);
          }
        }
    );
  }

  enableEdit(index: number) {
    this.editState.active = true;
    this.editState.index = index;
    this.updateView();
  }

  disableEdit() {
    this.editState.active = false;
    this.editState.index = null;
    this.ingredientForm.reset();
  }

  updateView() {
    const ingredient: Ingredient = this.shoppingListService.getIngredient(this.editState.index);
    this.ingredientForm.form.patchValue({
      name: ingredient.name,
      amount: ingredient.amount,
      unit: ingredient.unit
    });
  }

  onSubmit() {
    const formValues = this.ingredientForm.value;
    const ingredient = new Ingredient(formValues.name, formValues.amount, formValues.unit);

    if (this.editState.active) {
      this.shoppingListService.updateIngredient(this.editState.index, ingredient);
      this.shoppingListService.editIngredientToggled.next(this.editState.index);
    } else {
      this.shoppingListService.addIngredient(ingredient);
      this.ingredientForm.reset();
    }
  }

  onDelete() {
    if (this.editState.active) {
      this.shoppingListService.removeIngredient(this.editState.index);
    }
  }

  onClear() {
    this.ingredientForm.reset();
  }

}
