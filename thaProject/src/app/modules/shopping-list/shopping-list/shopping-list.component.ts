import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Ingredient } from '../../../models/ingredient';
import { ShoppingListService } from '../../core/services/shopping-list.service';


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients: Ingredient[];
  chosenItem: number;

  private ingredientListSub: Subscription;

  constructor(private shoppingListService: ShoppingListService) { }

  ngOnInit() {
    this.ingredients = this.shoppingListService.getIngredientList();
    this.ingredientListSub = this.shoppingListService.ingredientListChanged.subscribe(
      () => {
        this.ingredients = this.shoppingListService.getIngredientList();
        console.log(this.ingredients);
      }
    );

    this.shoppingListService.editIngredientToggled
      .subscribe(
        (index) => {
          this.chosenItem = this.chosenItem === index ? null : index;
        }
      );
  }

  onItemClicked(index: number) {
    this.shoppingListService.editIngredientToggled.next(index);
  }

  ngOnDestroy(): void {
    if (this.ingredientListSub) {
      this.ingredientListSub.unsubscribe();
    }
  }

}
