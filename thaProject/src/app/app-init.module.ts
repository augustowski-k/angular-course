import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthService } from './modules/core/services/auth.service';
import { RecipeService } from './modules/core/services/recipe.service';

export function get_recipes(recipeService: RecipeService) {
  return () => recipeService.initRecipes();
}

export function init_auth(authService: AuthService) {
  return () => authService.initialize();
}

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    {
      provide: APP_INITIALIZER, useFactory: init_auth, deps: [AuthService], multi: true
    },
    {
      provide: APP_INITIALIZER, useFactory: get_recipes, deps: [RecipeService], multi: true
    },
  ]
})
export class AppInitModule { }
