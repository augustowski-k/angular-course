import {
  Component,
  OnInit
} from '@angular/core';

import { RecipeService } from './modules/core/services/recipe.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private recipeService: RecipeService) {}

  ngOnInit() {}
}
